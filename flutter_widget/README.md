# Flutter Widget

Materi ini merupakan kelanjutan dari materi "Flutter Introduction". Pada materi ini, kita akan membahas lebih lanjut mengenai Stateless & Stateful Widget, Built-in Widget (Scaffold, AppBar, Text), dan Platform (MaterialApp, CupertinoApp).

Link materi:

- [Flutter Widget](https://docs.google.com/presentation/d/1FEonn2L-zdcoKWQBOJAhxqTqfxLrrP5ZBHAbJuqhJBw/edit#slide=id.g102d5aad581_0_44?usp=sharing)
- [Flutter Platform Widget](https://docs.google.com/presentation/d/1tGIomdZbdtPOrjCK0Bj2xVfStGExM-13pMUFPC2kCFA/edit?usp=sharing)

Referensi:

- [AppBar](https://blog.logrocket.com/flutter-appbar-tutorial/)
- [Drawer](https://blog.logrocket.com/how-to-add-navigation-drawer-flutter/)
- [Bottom Navigation Bar](https://blog.logrocket.com/how-to-build-a-bottom-navigation-bar-in-flutter/)
